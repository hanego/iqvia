import random
import string
from datetime import datetime, timedelta

from celery import Celery

from api import db, Contact

app = Celery('tasks', broker='redis://127.0.0.1:6379/0') # supposing redis is on "redis" server


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    db.create_all() # create the table as it s an in memory table, in a real life example, this wouldn t be necessary

    sender.add_periodic_task(15, create_contact.s(), name='add contact every 15s')

    sender.add_periodic_task(60, delete_old_contacts.s(), name='delete old contacts every 60s')

    sender.add_periodic_task(5, display.s(), name="display the existing contains every 5s")


@app.task
def create_contact(*args, **kwargs):
    db.session.add(Contact(username=randomString(), firstName=randomString(), lastName=randomString()))
    db.session.commit()


@app.task
def delete_old_contacts(*args, **kwargs):
    Contact.query.filter(Contact.createdAt < datetime.now() - timedelta(seconds=60)).delete()
    db.session.commit()


@app.task
def display(*args, **kwargs):
    print(Contact.query.all())
