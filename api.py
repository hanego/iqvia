import traceback
from datetime import datetime

from flask import Flask, jsonify, request, make_response, abort
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, validates

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = r'sqlite://'
db = SQLAlchemy(app)

LOGGER = app.logger

CONTACT_NECESSARY_PROPERTIES = ["username", "firstName", "lastName"] # possibility to use mapper but more controls with hardcoded properties


class Contact(db.Model):
    username = db.Column(db.String(80), primary_key=True)
    firstName = db.Column("first_name", db.String(120), nullable=False)
    lastName = db.Column("last_name", db.String(120), nullable=False)
    createdAt = db.Column(db.DateTime, default=datetime.utcnow)

    emails = relationship("Email", cascade="all, delete, delete-orphan")

    def to_json(self):
        return {
            "username": self.username,
            "firstName": self.firstName,
            "lastName": self.lastName,
            "emails": [email.email for email in  self.emails]
        }

    def __repr__(self):
        return '<Contact %r>' % self.username


class Email(db.Model):
    email = db.Column(db.String(120), primary_key=True)
    username = db.Column(db.String(80), ForeignKey(Contact.username), nullable=False)

    contact = relationship(Contact)

    @validates("email")
    def validate_email(self, key, field):
        assert "@" in field # a more accurate email validation might be necessary (with a regular expression for example)
        return field

    def to_json(self):
        return {
            "email": self.email,
            "contact": self.contact.to_json()
        }

    def __repr__(self):
        return '<Contact %r>' % self.username


def empty_response():
    response = make_response("", 204)
    response.mimetype = app.config["JSONIFY_MIMETYPE"]
    return response


@app.errorhandler(404)
def handle_not_found(e):
    db.session.rollback()
    response = make_response(jsonify({"message": "Entity was not found."}), 404)
    response.mimetype = app.config["JSONIFY_MIMETYPE"]
    return response


@app.errorhandler(409)
def handle_conflict(e):
    db.session.rollback()
    response = make_response(jsonify({"message": "Already exists"}), 409)
    response.mimetype = app.config["JSONIFY_MIMETYPE"]
    return response


@app.errorhandler(500)
@app.errorhandler(Exception)
def handle_unknown_error(e):
    LOGGER.error(repr(e))
    LOGGER.error(traceback.format_exc())
    db.session.rollback()
    response = make_response(jsonify({"message": "Unknown error"}), 500)
    response.mimetype = app.config["JSONIFY_MIMETYPE"]
    return response


@app.route("/api/contacts", methods=["GET"])
def list_users():
    users = [contact.to_json() for contact in Contact.query.all()]

    return jsonify(users)


@app.route("/api/contacts/<username>", methods=["GET"])
def get_contact(username):
    contact = Contact.query.get(username)
    if not contact:
        abort(404)

    return jsonify(contact.to_json())


@app.route("/api/contacts", methods=["POST"])
def create_contact():
    data = request.get_json()

    contact = Contact()

    for property in CONTACT_NECESSARY_PROPERTIES:
        if property not in data:
            raise ValueError("{property} field not found.".format(property))
        setattr(contact, property, data[property])

    _contact = Contact.query.get(contact.username)
    if _contact:
        abort(409)

    if "emails" in data:
        for email in data["emails"]:
            contact.emails.append(Email(email=email, username=contact.username))

    db.session.add(contact)
    db.session.commit()

    return jsonify(contact.to_json())


@app.route("/api/contacts/<username>", methods=["PUT"])
def update_contact(username):
    contact = Contact.query.get(username)
    if not contact:
        abort(404)

    data = request.get_json()

    for name, value in data.items():
        if name in ["firstName", "lastName"]:
            setattr(contact, name, value)
        elif name == "emails":
            for email in contact.emails:
                db.session.delete(email)

            for email in value:
                contact.emails.append(Email(email=email))

    db.session.commit()

    return empty_response()


@app.route("/api/contacts/<username>", methods=["DELETE"])
def delete_contact(username):
    contact = Contact.query.get(username)
    if not contact:
        abort(404)

    db.session.delete(contact)
    db.session.commit()

    return empty_response()


# for the exercise only
if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)