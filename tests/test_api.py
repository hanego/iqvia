import json
import unittest

from api import app, Contact, Email, db


class TestApi(unittest.TestCase):

    default_contact = {"username": "agent007", "firstName": "James", "lastName": "Bond"}
    default_emails = ["james.bond@gmail.com", "james@mi6.co.uk"]

    @classmethod
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

        db.reflect()
        db.drop_all()
        db.create_all()
        contact = Contact(**self.default_contact)
        for email in self.default_emails:
            db.session.add(Email(email=email, username=contact.username))

        db.session.add(contact)
        db.session.commit()


    def test_get_contacts(self):
        resp = self.app.get("/api/contacts", content_type='application/json')
        data = json.loads(resp.get_data(as_text=True))
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]["username"], "agent007")

    def test_create_contact(self):
        data = {
            "username": "bob",
            "firstName": "Bob",
            "lastName": "Fraser",
            "emails": ["bob.fraser@example.com"]
        }
        resp = self.app.post("/api/contacts", data=json.dumps(data), content_type='application/json')
        _data = json.loads(resp.get_data(as_text=True))
        self.assertEqual(_data, data)
        contact = Contact.query.get(data["username"])
        self.assertIsNotNone(contact)
        self.assertEqual(contact.firstName, data["firstName"])

    def test_create_contact_with_existing_email(self):
        data = {
            "username": "bob",
            "firstName": "Bob",
            "lastName": "Fraser",
            "emails": self.default_emails
        }
        resp = self.app.post("/api/contacts", data=json.dumps(data), content_type='application/json')
        self.assertEqual(resp.status_code, 500)

    def test_existing_contact_create(self):
        resp = self.app.post("/api/contacts", data=json.dumps(self.default_contact), content_type='application/json')
        self.assertEqual(resp.status_code, 409)

    def test_delete_contact(self):
        resp = self.app.delete("/api/contacts/{username}".format(username=self.default_contact["username"]), content_type='application/json')
        self.assertEqual(resp.status_code, 204)
        contact = Contact.query.get(self.default_contact["username"])
        self.assertIsNone(contact)

    def test_delete_unknown_contact(self):
        resp = self.app.delete("/api/contacts/{username}".format(username="bob"),
                               content_type='application/json')
        self.assertEqual(resp.status_code, 404)

    def test_update_contact(self):
        data = {"firstName": "Bill"}
        resp = self.app.put(
            "/api/contacts/{username}".format(username=self.default_contact["username"]),
            data=json.dumps(data),
            content_type='application/json'
        )

        self.assertEqual(resp.status_code, 204)
        contact = Contact.query.get(self.default_contact["username"])
        self.assertEqual(contact.firstName, data["firstName"])

    def test_update_unknown_contact(self):
        resp = self.app.put(
            "/api/contacts/{username}".format(username="bob"),
            data=json.dumps(self.default_contact),
            content_type='application/json'
        )
        self.assertEqual(resp.status_code, 404)





